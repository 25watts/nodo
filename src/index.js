import 'bootstrap'
import './sass/main.scss'
import $ from "jquery";
import 'owl.carousel';
import "owl.carousel/dist/assets/owl.carousel.css";
// import '@fortawesome/fontawesome-free/js/fontawesome'
// import '@fortawesome/fontawesome-free/js/solid'
// import '@fortawesome/fontawesome-free/js/regular'
// import '@fortawesome/fontawesome-free/js/brands'

$(document).ready(function(){

    var width = $(window).width();
    /*
    if (width < 780){
      $('.nav-item a, .navbar-brand').click(function(){
        $('.navbar-toggle').toggleClass('open');
        $('header').toggleClass('open');      
      });
    }
    */
    $('#navbar-toggle').click(function(){
        $(this).toggleClass('open');
        $('header').toggleClass('open');      
    });
    
    /* scroll change background */
    var scroll_pos = 0;
    $(document).scroll(function() { 
        scroll_pos = $(this).scrollTop();
        if(scroll_pos > 120) {
            $(".banner").addClass('scrolled');
            $(".logo").addClass('d-none');
            $(".logo-black").addClass('d-block');
            $(".elipse2").addClass('d-none');
        } else {
            $(".banner").removeClass('scrolled');
            $(".logo").removeClass('d-none');
            $(".logo-black").removeClass('d-block');
            $(".elipse2").removeClass('d-none');
        }
    });

    $("a").on('click', function(event) {

        //ocultar menu al hacer click
        $("header").removeClass('open');
        // Make sure this.hash has a value before overriding default behavior
        if (this.hash !== "") {
          // Prevent default anchor click behavior
          event.preventDefault();
    
          // Store hash
          var hash = this.hash;
    
          // Using jQuery's animate() method to add smooth page scroll
          // The optional number (800) specifies the number of milliseconds it takes to scroll to the specified area
          $('html, body').animate({
            scrollTop: $(hash).offset().top
          }, 800, function(){
    
            // Add hash (#) to URL when done scrolling (default click behavior)
            window.location.hash = hash;
          });
        } // End if
    });

    $('.owl-carousel').owlCarousel({
      loop:false,
      nav: true,
      //navText: ['<i class="fas fa-chevron-left"></i>','<i class="fas fa-chevron-right"></i>'],
      //navContainer: '.container .owl-theme .owl-controls .custom-nav',
      mouseDrag:true,
      autoplay:true,
      responsive : {
        0 : {
          items:1,
        },
        991 : {
          items: 1,
        },
        1200 : {
          items: 1,
        }
    }
    });

    const myForms = document.querySelector("#form-validate");
      myForms.addEventListener("submit", function(e) {
        e.preventDefault();
      const name    = document.querySelector("#name").value;
      const email   = document.querySelector("#email").value;
      const phone   = document.querySelector("#phone").value;
      const localidad = document.querySelector("#localidad").value;
      const message = document.querySelector("#message").value;
      const btn     = document.querySelector("#btn-send");
      const succes  = document.querySelector("#alert-suc");
      const danger  = document.querySelector("#alert-dan");
      const data = {
          "from": `info@nodomedia.com.ar`,
          "to" : "info@nodomedia.com.ar,ms@rv.com.ar,gonzalo@nodomedia.com.ar",
          "subject": "Nodo Consulta",
          "html":`<b>Tiene una consulta</b><br><br>Nombre: body.name. <br>Telefono: body.phone <br> Email: body.email <br> Localidad: body.localidad <br> Mensaje: body.message`,
          "body": 
          {
              "name": `${name}`,
              "email": `${email}`,
              "phone": `${phone}`,
              "localidad" : `${localidad}`,
              "message": `${message}`
          }
      }
      var settings = {
          "async": true,
          "crossDomain": true,
          "url": "https://api.25watts.com.ar/api/core/email",
          "method": "POST",
          "headers": {
          "Content-Type": "application/json",
          "Authorization": "eyJhbGciOiJIUzI1NiJ9.MjV3YXR0cw.kgSoQDlnQzExTdk0tsuuckQv-AQtabtDSc5weSQZ6CQ",
          "cache-control": "no-cache",
          },
          "processData": false,
          "data": JSON.stringify(data),
          }
          $.ajax(settings).done(function (response) {
          console.log(response.type);
          if (response.type !== "success") {
              danger.classList.add("show");
              succes.classList.remove("alert-hidden");
              setTimeout( () => {
              succes.classList.add("alert-hidden");
              }, 3000);
          } else {
              myForms.reset();
              succes.classList.add("show");
              succes.classList.remove("alert-hidden");
              setTimeout( () => {
              succes.classList.add("alert-hidden");
              }, 3000);
          }
          });
      });

    $("#contacto-link").click(function() {
        $("html, body").animate({
            scrollTop: $("#contacto").offset().top
        }, 1000)
    });
});
